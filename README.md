# README #

This README contains specific information for HeaRTDroid rules engine.

### Essential files ###

Before launching the app, you need to upload an .hmr file with a model into phone/tablet memory. By default, the file must be placed in internal storage (not on the memory card) and must be placed inside "heartdroid" directory. The sample model file is available in the "Downloads" section.

### Useful link ###
You can find more detailed information about using HeaRTDroid at https://glados.kis.agh.edu.pl/doku.php?id=pub:software:heartdroid:start.