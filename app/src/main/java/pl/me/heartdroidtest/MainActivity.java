package pl.me.heartdroidtest;

import android.Manifest;
import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import heart.Configuration;
import heart.HeaRT;
import heart.State;
import heart.StateElement;
import heart.alsvfd.SimpleNumeric;
import heart.exceptions.AttributeNotRegisteredException;
import heart.exceptions.BuilderException;
import heart.exceptions.ModelBuildingException;
import heart.exceptions.NotInTheDomainException;
import heart.exceptions.ParsingSyntaxException;
import heart.parser.hmr.HMRParser;
import heart.parser.hmr.runtime.SourceFile;
import heart.xtt.XTTModel;

public class MainActivity extends Activity {

//    private static final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+"/heartdroid/";
    private TextView textView;
    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;

    private static final String PATH = Environment.getExternalStorageDirectory().getAbsolutePath()+
            "/heartdroid/psc-zatrudnienie-ocena_kandydata.pl";
    private XTTModel model;
    private SourceFile sourceFile;
    private HMRParser parser;

    public static StringBuilder textBuffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textview);
        int permissionCheck;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            permissionCheck = this.checkSelfPermission("Manifest.permission.READ_EXTERNAL_STORAGE");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1001); //Any number
            }
        }
    }

    /**
     * Method is called when the button on the screen is clicked
     * @param view
     */
    public void go (View view) {
        System.out.println("Clicked Go!");
//        ifContinue = true;
        textView.setKeepScreenOn(true);
//        textBuffer = new StringBuilder();

//        try {
//            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
//                    .getAbsolutePath()+"/benchmark/heartdroid.txt",true));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        try {
            // - - - - - - - - Loading a model to HeaRTDroid - - - - - - - - //

            //Loading a file with a model
            sourceFile = new SourceFile(PATH);
            parser = new HMRParser();

            //Parsing the file with the model
            parser.parse(sourceFile);
            model = parser.getModel();

            // - - - - - - - - Creating a new state (initial state) - - - - - - - - //

            // Creating StateElements objects, one for each attribute
            StateElement ocenaKwalifikacjiE = new StateElement();
            StateElement ocenaRozmowyE = new StateElement();
            StateElement ocenaTestowE = new StateElement();

            // Setting the values of the state elements
            ocenaKwalifikacjiE.setAttributeName("ocenaKwalifikacji");
            ocenaKwalifikacjiE.setValue(new SimpleNumeric(5.0));

            ocenaRozmowyE.setAttributeName("ocenaRozmowy");
            ocenaRozmowyE.setValue(new SimpleNumeric(2.0));

            ocenaTestowE.setAttributeName("ocenaTestow");
            ocenaTestowE.setValue(new SimpleNumeric(2.0));

            //Creating a XTTState object that agregates all the StateElements
            State XTTstate = new State();
            XTTstate.addStateElement(ocenaKwalifikacjiE);
            XTTstate.addStateElement(ocenaRozmowyE);
            XTTstate.addStateElement(ocenaTestowE);

            // - - - - - - - - - Running the inference - - - - - - - - //

            //measuring block
//        ifContinue = true;
//        new Monitor().execute();
//        start = new Date();
            //measuring block

            // Fixed order inference -- we give all tables names
            // in an order in which they should be fired
            HeaRT.fixedOrderInference(model,
                    new String[]{"ocenakandydata"},
                    new Configuration.Builder()
                            .setInitialState(XTTstate)
                            .build());

            // Data driven inference -- we give only the starting tables names.
            // The algorithm crawls the table network and fires only the necessary tables.
//            HeaRT.dataDrivenInference(model,
//                    new String[]{"DayTime","Today"},
//                    new Configuration.Builder()
//                            .setInitialState(XTTstate)
//                            .build());

            // Goal inference -- we only give the table which produces the attribute value
            // that we are interested in.
            // The algorithm crawls the table network and fires only the necessary tables.
//            HeaRT.goalDrivenInference(model,
//                    new String[]{"Threats"},
//                    new Configuration.Builder()
//                            .setInitialState(XTTstate)
//                            .build());

//        end = new Date();
//        ifContinue = false;

            System.out.println("Printing current state (after inference)");
            State endState = HeaRT.getWm().getCurrentState(model);
            for(StateElement se : endState){
                System.out.println("Attribute "+se.getAttributeName()+" = "+se.getValue());
            }

            textView.setText("State after inference\n");
            for(StateElement se : endState){
                textView.setText(textView.getText()+"Attribute "+se.getAttributeName()+" = "+se.getValue()+"\n");
            }

        } catch (ModelBuildingException e) {
            e.printStackTrace();
        } catch (ParsingSyntaxException e) {
            e.printStackTrace();
        } catch (BuilderException e) {
            e.printStackTrace();
        } catch (NotInTheDomainException e) {
            e.printStackTrace();
        } catch (AttributeNotRegisteredException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
        textView.setKeepScreenOn(false);
        System.out.println("End of go()");
//        textView.setText(textBuffer.toString());
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write(printDateTime());
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf( (maxProc-minProc)*100));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }
}
